package com.ucbcba.edu.bo.Modules;

public class StringManager {

    public static int letters_quantity(String text) {
        String abc="abcdefghijklmnopqrstuvwxyz";
        int quantity=0;
        String lower_cassed=text.toLowerCase();
        for (char car:lower_cassed.toCharArray()) {
            if (abc.indexOf(car)>=0)
            {
                quantity++;
            }
        }
        return quantity;
    }

    public static int numbers_quantity(String text) {
        String abc="0123456789";
        int quantity=0;
        for (char car:text.toCharArray()) {
            if (abc.indexOf(car)>=0)
            {
                quantity++;
            }
        }
        return quantity;
    }

    public static int special_char_quantity(String text) {
        String special=text.replaceAll("[a-zA-Z0-9 ]", "");

        return special.length();
    }

    public static int words_quantity(String text)
    {
        String[] words=text.split(" ");
        return words.length;
    }
}
