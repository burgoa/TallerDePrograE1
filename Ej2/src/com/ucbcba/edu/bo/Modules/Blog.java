package com.ucbcba.edu.bo.Modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Blog extends WebSite{
    private List<Article> articles;
    private User user;
    public Blog(String url, String name) {
        super(url, name);
        articles=new ArrayList<>();
    }
    public void add_article(Article article)
    {
        articles.add(article);
    }
    public void delete_article(int Id)
    {
        if ((Id-1)>=0 && (Id-1<articles.size())) {
            articles.remove(Id-1);
        }
    }
    public int articles_quantity()
    {
        return articles.size();
    }

    public int comments_quantity()
    {
        int quantity=0;
        for (Article article:articles) {
            quantity+=article.comments_quantity();
        }
        return quantity;
    }

    public int likes_quantity()
    {
        int quantity=0;
        for (Article article:articles) {
            quantity+=article.getLikes();
        }
        return quantity;
    }

    public int words_quantity()
    {
        return 0;
    }


    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public int letters_quantity() {
        int quantity=0;
        for (Article article:articles) {
            quantity+=article.letters_quantity();
        }
        return quantity;
    }

    public int numbers_quantity() {
        int quantity=0;
        for (Article article:articles) {
            quantity+=article.numbers_quantity();
        }
        return quantity;
    }

    public int special_char_quantity() {
        int quantity=0;
        for (Article article:articles) {
            quantity+=article.special_char_quantity();
        }
        return quantity;
    }

    public void show_articles(){
        System.out.println("Nombre "+getName());
        for (Article article:articles) {
            article.print();
        }
    }

}
