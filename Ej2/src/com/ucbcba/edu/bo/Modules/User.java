package com.ucbcba.edu.bo.Modules;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String last_name;
    private List<Blog> blogs;
    private List<Article> articles;
    private List<Comment> comments;
    public User(String name, String last_name) {
        this.name = name;
        this.last_name = last_name;
        blogs=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
    public String get_complete_username(){
        return name+" "+last_name;
    }

    @Override
    public String toString()
    {
        return get_complete_username();
    }

}
