package com.ucbcba.edu.bo.Modules;

import com.ucbcba.edu.bo.Exceptions.UserNotFoundException;

public class Dislike {
    public static void dislike(Article article, String user_name) throws UserNotFoundException {
        User user=article.find_user(user_name);
        if (user!=null)
        {
           article.remove_like(user);
        }
        if (user==null)
        {
            throw new UserNotFoundException("User not found");
        }
    }

    public static void dislike(Comment comment, String user_name) throws UserNotFoundException {
        User user=comment.find_user(user_name);
        if (user!=null)
        {
            comment.remove_like(user);
        }
        if (user==null)
        {
            throw new UserNotFoundException("User not found");
        }
    }
}
