package com.ucbcba.edu.bo.Modules;

import com.ucbcba.edu.bo.Exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class Article {
    private String text;
    private Blog blog;
    private User author;
    private List<Comment> comments;
    private List<User> like;
    public Article(String text) {
        this.text = text;
        comments=new ArrayList<>();
        like=new ArrayList<>();
    }

    public void add_comment(Comment comment)
    {
        comments.add(comment);
    }
    public void delete_comment(int Id){
        if ((Id-1)>=0 && (Id-1<comments.size())) {
            comments.remove(Id-1);
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLikes() {
        return like.size();
    }


    public int comments_quantity()
    {
        return comments.size();
    }

    public int words_quantity()
    {
        return StringManager.words_quantity(text);
    }

    public void like(User user)
    {
        if (!like.contains(user))
        {
            like.add(user);
        }
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public int letters_quantity() {
        int quantity=0;
        for (Comment comment:comments) {
            quantity+=comment.letters_quantity();
        }
       return StringManager.letters_quantity(text)+quantity;
    }

    public int numbers_quantity() {
        int quantity=0;
        for (Comment comment:comments) {
            quantity+=comment.numbers_quantity();
        }
        return StringManager.numbers_quantity(text)+quantity;
    }

    public int special_char_quantity() {
        int quantity=0;
        for (Comment comment:comments) {
            quantity+=comment.special_char_quantity();
        }
        return StringManager.special_char_quantity(text)+quantity;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void print() {
        System.out.println("    Texto: "+text);
        System.out.println("    Likes: "+getLikes());
        print_likes();
        System.out.println("    Autor: "+ author.get_complete_username());
        System.out.println("    Comentarios: ");
        print_comments();

    }

    private void print_comments() {
        int count=1;
        for (Comment comment:comments){
            comment.print();

        }
    }

    private void print_likes()
    {
        String text=like.toString();
        text=text.replace("[","");
        text=text.replace("]","");
        System.out.println("        "+text);

    }
    //dislike
    public void dislike(String user_name) throws UserNotFoundException {
       Dislike.dislike(this, user_name);

    }
    public User find_user(String user_name)
    {
        for (var item:like) {
            if (item.getName().equals(user_name))
            {
                return item;
            }
        }
        return null;
    }
    public boolean user_has_like(User user)
    {
        return like.contains(user);
    }

    public void remove_like(User user) {
        like.remove(user);
    }
}