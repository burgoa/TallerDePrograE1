package com.ucbcba.edu.bo.Modules;

import com.ucbcba.edu.bo.Exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class Comment {
    private String text;
    private Article article;
    private User author;
    private List<User> like;
    public Comment(String text) {
        this.text = text;
        like=new ArrayList<>();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLikes() {
        return like.size();
    }


    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
    public int words_quantity()
    {
      return StringManager.words_quantity(text);
    }


    public void like(User user)
    {
        if (!like.contains(user))
        {
            like.add(user);
        }
    }



    public int letters_quantity() {
        return StringManager.letters_quantity(text);
    }

    public int numbers_quantity() {
        return StringManager.numbers_quantity(text);
    }

    public int special_char_quantity() {
        return StringManager.special_char_quantity(text);
    }


    public void print() {
        System.out.println("        Texto: "+ text);
        System.out.println("        Autor: "+ author.get_complete_username());
        System.out.println("        Likes: "+getLikes());
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    //Dislike
    public void dislike(String user_name) throws UserNotFoundException {
        Dislike.dislike(this, user_name);
    }

    public User find_user(String user_name)
    {
        for (var item:like) {
            if (item.getName().equals(user_name))
            {
                return item;
            }
        }
        return null;
    }

    public void remove_like(User user) {
        like.remove(user);
    }
}
