package com.ucbcba.edu.bo;

import com.ucbcba.edu.bo.Modules.Article;
import com.ucbcba.edu.bo.Modules.Blog;
import com.ucbcba.edu.bo.Modules.Comment;
import com.ucbcba.edu.bo.Modules.User;

public class Main {
    public static void main(String[] args)
    {
        ShowBlog();

    }

    private static void ShowBlog() {
        User user1=new User("Brandon", "Burgoa");
        User user2=new User("Chilly", "Willy");
        Blog blog=new Blog("google.com", "google");

        Article article1=new Article("Articulo 1");
        Article article2=new Article("Articulo 2");
        article1.like(user1);
        article1.like(user2);
        article1.setAuthor(user1);

        Comment comment1=new Comment("comentario 1");
        comment1.setAuthor(user2);
        Comment comment2=new Comment("comentario 2");
        comment2.setAuthor(user1);

        article1.add_comment(comment1);
        article1.add_comment(comment2);

        article2.like(user1);
        article2.like(user2);
        article2.setAuthor(user1);

        blog.add_article(article1);
        blog.add_article(article2);

        blog.show_articles();
    }
}
