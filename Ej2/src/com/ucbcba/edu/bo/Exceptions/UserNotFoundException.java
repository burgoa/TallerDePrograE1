package com.ucbcba.edu.bo.Exceptions;

public class UserNotFoundException extends Exception {
    private String text;
    public UserNotFoundException (String text){
        super(text);
    }
}
