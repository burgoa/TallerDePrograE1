package Test;

import com.ucbcba.edu.bo.Exceptions.UserNotFoundException;
import com.ucbcba.edu.bo.Modules.Article;
import com.ucbcba.edu.bo.Modules.Blog;
import com.ucbcba.edu.bo.Modules.Comment;
import com.ucbcba.edu.bo.Modules.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommentTest {
    Comment comment;
    Article article;
    Blog blog;
    User user;
    User other;
    @Before
    public void init(){
        blog= new Blog("google.com", "google");
        article=new Article("Es un articulo");
        article.setBlog(blog);
        comment=new Comment("Me encantº1");
        comment.setArticle(article);
        user=new User("Brandon", "Burgoa");
        other=new User("lopolafa", "olaf");
    }
    @Test
    public void IfIALikeInTwoMomentsShouldBeReturn1(){
        comment.like(user);
        comment.like(user);
        Assert.assertEquals(comment.getLikes(), 1);
    }
    @Test
    public void IfLikeTwoUsersShouldBeReturn2(){
        comment.like(user);
        comment.like(other);
        Assert.assertEquals(comment.getLikes(), 2);
    }
    @Test
    public void QuantityOfWordsShouldBeFour()
    {
        Assert.assertEquals(article.words_quantity(), 3);
    }

    @Test
    public void QuantityOfWordsShouldBeReturn8(){
        Assert.assertEquals(comment.letters_quantity(), 8);
    }
    @Test
    public void QuantityOfNumbersShouldBeReturn1(){
        Assert.assertEquals(comment.numbers_quantity(), 1);
    }
    @Test
    public void QuantityOfSpecialCharsShouldBeReturn1(){
        Assert.assertEquals(comment.special_char_quantity(), 1);
    }
    @Test
    public void IfILikeAnCommentAndDislikeNotShouldContainsThisUser() throws UserNotFoundException {
        User user=new User("Brandon", "Burgoa");
        comment.like(user);
            comment.dislike( "Brandon");
        Assert.assertEquals(article.user_has_like(user), false);
    }
}
