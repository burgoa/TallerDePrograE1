package Test;

import com.ucbcba.edu.bo.Modules.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class BlogTest {
    Blog blog;
    Article article;
    Comment comment;
    User user;
    User other;
    @Before
    public void init(){
        blog=new Blog("google.com", "google");
        user=new User("Brandon", "Burgoa");
        other=new User("lopolafa", "olaf");
    }
    @Test
    public void AddAndRemoveArticle()
    {
        article=new Article("Este es un articulo");
        article.setBlog(blog);
        blog.add_article(article);
        Assert.assertEquals(blog.articles_quantity(), 1);
        blog.delete_article(1);
        Assert.assertEquals(blog.articles_quantity(), 0);
    }
    @Test
    public void RemoveInexistentArticle()
    {
        blog.delete_article(1);
        Assert.assertEquals(blog.articles_quantity(), 0);
    }

    @Test
    public void IfIAddTwoArticlesAndLikedBothShouldBeReturn2(){
        article=new Article("Este es un articulo");
        article.setBlog(blog);
        blog.add_article(article);
        article.like(user);

        article=new Article("Este es un articulo");
        article.setBlog(blog);
        blog.add_article(article);
        article.like(other);

        Assert.assertEquals(blog.likes_quantity(), 2);
    }

    @Test
    public void IfIAddTwoArticlesAndCommentBothShouldBeReturnTwoCommentsOnTheBlog(){
        article=new Article("Este es un articulo 1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);


        article=new Article("Este es un articulo 2");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("No me gusto");
        comment.setArticle(article);
        article.add_comment(comment);


        Assert.assertEquals(blog.comments_quantity(), 2);
    }
    @Test
    public void AddTwoArticlesShouldBeReturn8(){
        article=new Article("Este es un articulo");
        article.setBlog(blog);
        blog.add_article(article);
        article=new Article("Este es un articulo");
        article.setBlog(blog);
        blog.add_article(article);

        Assert.assertEquals(blog.words_quantity(), 0);
    }
    @Test
    public void QuantityOfWordsShouldBeReturn46(){

        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);


        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("No me gusto");
        comment.setArticle(article);
        article.add_comment(comment);


        Assert.assertEquals(blog.letters_quantity(), 46);
    }
    @Test
    public void QuantityOfNumbersShouldBeReturn4(){
        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);
        blog.add_article(article);

        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);
        blog.add_article(article);
        Assert.assertEquals(blog.numbers_quantity(), 4);
    }
    @Test
    public void QuantityOfSpecialCharsShouldBeReturn4(){
        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);
        blog.add_article(article);

        article=new Article("Este es un articulº1");
        article.setBlog(blog);
        blog.add_article(article);
        comment=new Comment("Me gusto");
        comment.setArticle(article);
        article.add_comment(comment);
        blog.add_article(article);
        Assert.assertEquals(blog.special_char_quantity(), 4);
    }
}
