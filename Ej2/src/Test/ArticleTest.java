package Test;

import com.ucbcba.edu.bo.Exceptions.UserNotFoundException;
import com.ucbcba.edu.bo.Modules.Article;
import com.ucbcba.edu.bo.Modules.Blog;
import com.ucbcba.edu.bo.Modules.Comment;
import com.ucbcba.edu.bo.Modules.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ArticleTest {
    Article article;
    Blog blog;
    Comment comment;
     User user;
     User other;

    @Before
    public void init(){
        blog=new Blog("google.com", "google");
        article =new Article("Este es un articulº1");
        article.setBlog(blog);
        comment=new Comment("No me gustº1");
        comment.setArticle(article);
        article.add_comment(comment);
        user=new User("Brandon", "Burgoa");
        other=new User("lopolafa", "olaf");

    }
    @Test
    public void AddAndRemoveComment()
    {

        Assert.assertEquals(article.comments_quantity(), 1);
    }
    @Test
    public void RemoveInexistentComment()
    {
        article.delete_comment(1);
        article.delete_comment(1);
        Assert.assertEquals(article.comments_quantity(), 0);
    }
    @Test
    public void IfIALikeInTwoMomentsShouldBeReturn1(){
        comment.like(user);
        comment.like(user);
        Assert.assertEquals(comment.getLikes(), 1);
    }
    @Test
    public void IfLikeTwoUsersShouldBeReturn2(){
        comment.like(user);
        comment.like(other);
        Assert.assertEquals(comment.getLikes(), 2);
    }
    @Test
    public void QuantityOfWordsShouldBeFour()
    {
        Assert.assertEquals(article.words_quantity(), 4);
    }
    @Test
    public void QuantityOfWordsShouldBeReturn23(){
        Assert.assertEquals(article.letters_quantity(), 23);
    }
    @Test
    public void QuantityOfNumbersShouldBeReturn2(){
        Assert.assertEquals(article.numbers_quantity(), 2);
    }
    @Test
    public void QuantityOfSpecialCharsShouldBeReturn2(){
        Assert.assertEquals(article.special_char_quantity(), 2);
    }
    @Test
    public void IfILikeAnArticleAndDislikeNotShouldContainsThisUser() throws UserNotFoundException {
        User user=new User("Brandon", "Burgoa");
        article.like(user);
        article.dislike( "Brandon");
        Assert.assertEquals(article.user_has_like(user), false);
    }
}
