package Test;

import com.ucbcba.edu.bo.Interfaces.Figure;
import com.ucbcba.edu.bo.Implementations.Square;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SquareTest {
    Figure figure;
    @Before
    public void init(){
        figure=new Square(5);
    }

    @Test
    public void Area()
    {
        Assert.assertEquals(figure.area(), 25, 2);
    }
    @Test
    public void Perimeter()
    {
        Assert.assertEquals(figure.perimeter(), 20, 3);
    }
}
