package Test;

import com.ucbcba.edu.bo.Implementations.Circle;
import com.ucbcba.edu.bo.Interfaces.Figure;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CircleTest {
    Figure figure;
    @Before
    public void init(){
     figure=new Circle(5);
    }
    @Test
    public void Area(){
        Assert.assertEquals(figure.area(),78.54, 2);
    }
    @Test
    public void Perimeter(){
        Assert.assertEquals(figure.perimeter(),31.416, 3);
    }

}