package com.ucbcba.edu.bo.Interfaces;

public interface Figure extends Drawable {
    double perimeter();
    double area();
}
