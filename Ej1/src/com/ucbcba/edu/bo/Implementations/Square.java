package com.ucbcba.edu.bo.Implementations;

import com.ucbcba.edu.bo.Interfaces.Figure;

public class Square implements Figure {
    private int width;

    public Square(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public double perimeter() {
        return 4*width;
    }

    @Override
    public double area() {
        return Math.pow(width, 2);
    }

    @Override
    public void draw() {
        System.out.println("-----SQUARE------");
        System.out.println("Perimeter: "+perimeter());
        System.out.println("Area: "+area());
        System.out.println("-----------DRAW SQUARE-----------");
        print_figure();
        System.out.println("--------------");
    }

    public void print_figure() {
        char[][] figure=create_figure();
        for (char[]row:figure) {
            for (char element:row) {
                System.out.print(element);
            }
            System.out.print('\n');
        }
    }

    public char[][] create_figure()
    {
        char[][] mat=new char[width][width];
        for (char[]row:mat) {
            for (int i=0; i<row.length;i++)
            {
                row[i]='*';
            }

        }
        return mat;
    }
}
