package com.ucbcba.edu.bo.Implementations;

import com.ucbcba.edu.bo.Interfaces.Figure;

public class Circle implements Figure {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double perimeter() {
        return 2* Math.PI*radius;
    }

    @Override
    public double area() {
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public void draw() {
        System.out.println("-----CIRCLE------");
        System.out.println("Perimeter: "+perimeter());
        System.out.println("Area: "+area());
        System.out.println("--------------");

    }
}
