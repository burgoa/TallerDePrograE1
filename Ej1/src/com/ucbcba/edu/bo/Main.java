package com.ucbcba.edu.bo;

import com.ucbcba.edu.bo.Implementations.Circle;
import com.ucbcba.edu.bo.Implementations.Square;
import com.ucbcba.edu.bo.Interfaces.Drawable;

public class Main {
    public static void main(String[] args)
    {
        draw(new Circle(5));
        draw(new Square(5));
    }
    public static void draw(Drawable drawable)
    {
        drawable.draw();
    }
}
