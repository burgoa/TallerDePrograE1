package com.ucbcba.edu.bo.Exceptions;

public class WrongPlayException extends Exception {
    public WrongPlayException(String msg){
        super(msg);
    }

}
