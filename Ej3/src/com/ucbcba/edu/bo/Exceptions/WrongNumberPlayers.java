package com.ucbcba.edu.bo.Exceptions;

public class WrongNumberPlayers extends Exception {
    public WrongNumberPlayers(String msg){
        super(msg);
    }
}
