package com.ucbcba.edu.bo;


import com.ucbcba.edu.bo.Exceptions.WrongNumberPlayers;
import com.ucbcba.edu.bo.Exceptions.WrongPlayException;
import com.ucbcba.edu.bo.Modules.RPT;

public class Main {
    public static void main(String[] args) throws WrongPlayException, WrongNumberPlayers {

        Tournament();

        Simple();

    }

    private static void Simple() throws WrongPlayException, WrongNumberPlayers {
        String[][] input = new String[][]{{"Juan", "R"}, {"Pedro", "R"}};
        RPT rpt=new RPT();
        String result=rpt.play(input);
        System.out.println(result);
    }

    private static void Tournament() throws WrongPlayException, WrongNumberPlayers {
        String [][]group1=new String[][]{{"Armando", "P"},{"Dave", "T"}};
        String [][]group2=new String[][]{{"Richard", "R"},{"Michael", "R"}};
        String [][][] Team1= new String[][][]{group1, group2};
        String [][]group3=new String[][]{{"Allen", "T"},{"Omer", "P"}};
        String [][]group4=new String[][]{{"David", "R"},{"Richard", "P"}};
        String [][][] Team2= new String[][][]{group3, group4};
        String [][][][] tournament= new String[][][][]{Team1, Team2};
        RPT rpt=new RPT();
        String winner=rpt.tournament(tournament);
        System.out.println(winner);
    }
}
