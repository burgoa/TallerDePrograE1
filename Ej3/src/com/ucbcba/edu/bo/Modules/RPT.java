package com.ucbcba.edu.bo.Modules;

import com.ucbcba.edu.bo.Exceptions.WrongNumberPlayers;
import com.ucbcba.edu.bo.Exceptions.WrongPlayException;

import java.util.ArrayList;
import java.util.List;

public class RPT {

    public RPT() {

    }

    public String play(String[][] input) throws WrongPlayException, WrongNumberPlayers {
        String [][][][] converted=new String[][][][]{new String[][][]{input}};
        verify(converted);
        String []result= play_core(input);
        String winner="["+result[0]+","+result[1]+"]";
        return winner;
    }




    //region Exercise 3
    public String[] play_core(String[][] input) throws WrongPlayException
    {
        String[] player1=input[0];
        String[] player2=input[1];
        switch (player1[1])
        {
            case "R":
                if (player2[1].equals("T"))
                {
                    return player1;
                }
                if (player2[1].equals("P"))
                {
                    return player2;
                }
                if (player2[1].equals("R"))
                {
                    return player1;
                }
            case "T":
                if (player2[1].equals("T"))
                {
                    return player1;
                }
                if (player2[1].equals("P"))
                {
                    return player1;
                }
                if (player2[1].equals("R"))
                {
                    return player2;
                }
            case "P":
                if (player2[1].equals("T"))
                {
                    return player2;
                }
                if (player2[1].equals("P"))
                {
                    return player1;
                }
                if (player2[1].equals("R"))
                {
                    return player1;
                }
        }
        throw new WrongPlayException("Incorrect input, you can only choose between R-P-T");
    }
    //endregion


    //region exercice 4
    private void verify(String[][][][] tournament) throws WrongNumberPlayers {
        List<String []> players=new ArrayList<>();
        for (var group:tournament) {
            for (var team:group) {
                for (var player:team) {
                    players.add(player);
                }
            }
        }
        if (isPotenceOfTwo(players.size()))
        {
            verifyNotEmpty(players);
        }
        else
        {
            throw new WrongNumberPlayers("The number of players is invalid");
        }
    }

    private void verifyNotEmpty(List<String[]> players) throws WrongNumberPlayers {
        for (var player:players) {
            if (player[0].equals("") || player[1].equals(""))
            {
                throw new WrongNumberPlayers("There are players or empty plays");
            }
        }

    }
    //endregion

    //region Exercise 5
    public String tournament(String[][][][] tournament) throws WrongPlayException, WrongNumberPlayers {
        verify(tournament);
        Node tree=tree(tournament);
        tree=fill_tournament(tree);
        String []result=tree.getInfo();
        String winner="["+result[0]+","+result[1]+"]";
        return winner;
    }



    public Node fill_tournament(Node tree)
    {
        Node left=tree.getLeft();
        Node right=tree.getRight();
        if (left==null || right==null)
        {
            return tree;
        }
        if (left!=null)
        {
            fill_tournament(left);
        }
        if (right!=null)
        {
            fill_tournament(right);
        }
        String [][]players=new String[][]{left.getInfo(),right.getInfo()};
        try {
            tree.setInfo(play_core(players));
        } catch (WrongPlayException e) {
            e.printStackTrace();
        }
        return tree;
    }

    public Node tree(String[][][][] tournament){
        List<Node> winners2=new ArrayList<>();
        for (var group:tournament) {
            List<Node> winners1=new ArrayList<>();
            for (var team:group){
                List<Node> nodes=new ArrayList<>();
                for (var player:team) {
                    Node node=new Node();
                    node.setInfo(player);
                    nodes.add(node);
                }
                Node win=new Node(nodes.get(0),nodes.get(1), null);
                winners1.add(win);
            }
            Node win=new Node(winners1.get(0),winners1.get(1), null);
            winners2.add(win);
        }
        Node tree=new Node(winners2.get(0),winners2.get(1), null);
        return tree;
    }
    public boolean isPotenceOfTwo(int size) {
        int mult=0;
        for(int i=1;Math.pow(2, i)<=size; i++)
        {
            if (Math.pow(2, i)==size)
            {
                return true;
            }
        }

        return false;
    }
    //endregion

}
