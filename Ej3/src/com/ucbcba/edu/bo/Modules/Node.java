package com.ucbcba.edu.bo.Modules;

public class Node {
    private Node left;
    private Node right;
    private String [] info;

    public Node(Node left, Node right, String[] info) {
        this.left = left;
        this.right = right;
        this.info = info;
    }
    public Node()
    {
        left=null;
        right=null;
        info=null;
    }

    public String[] getInfo() {
        return info;
    }

    public void setInfo(String[] info) {
        this.info = info;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
}
