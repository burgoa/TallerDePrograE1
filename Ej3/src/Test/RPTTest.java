package Test;

import com.ucbcba.edu.bo.Exceptions.WrongNumberPlayers;
import com.ucbcba.edu.bo.Exceptions.WrongPlayException;
import com.ucbcba.edu.bo.Modules.RPT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.fail;


public class RPTTest
{
    RPT rpt;
    String [][] input;
    @Before
    public void init(){
        rpt=new RPT();
    }
    @Test
    public void R_T_wonP1(){
        input=new String[][]{{"Juan", "R"},{"Pedro","T"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Juan", "R"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void R_P_wonP2(){
        input=new String[][]{{"Juan", "R"},{"Pedro","P"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Pedro", "P"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void P_R_wonP1(){
        input=new String[][]{{"Juan", "P"},{"Pedro","R"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Juan", "P"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void P_T_wonP2(){
        input=new String[][]{{"Juan", "P"},{"Pedro","T"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Pedro", "T"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void TP_wonP1(){
        input=new String[][]{{"Juan", "T"},{"Pedro","P"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Juan", "T"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void T_R_wonP2(){
        input=new String[][]{{"Juan", "T"},{"Pedro","R"}};
        try{
        Assert.assertEquals(rpt.play_core(input),new String[]{"Pedro", "R"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void SameElementWonFirst(){
        input=new String[][]{{"Juan", "R"},{"Pedro","R"}};
        try{
            Assert.assertEquals(rpt.play_core(input),new String[]{"Juan", "R"});
        }
        catch (Exception e)
        {

        }
    }
    @Test
    public void WrongPlayException(){
        input=new String[][]{{"Juan", "S"},{"Pedro","R"}};
        try{
            String[] result=rpt.play_core(input);
        }
        catch(WrongPlayException e)
        {
            return;
        }
        fail();
    }

    @Test
    public void TournamentOf2GroupsWith2Teams() throws WrongPlayException, WrongNumberPlayers {

        String[][][][] tournament = get_tournament();
        String winner=rpt.tournament(tournament);
        try{
            Assert.assertEquals(winner,"[Richard,R]");
        }
        catch (Exception e)
        {

        }
    }

    private String[][][][] get_tournament() {
        String [][]group1=new String[][]{{"Armando", "P"},{"Dave", "T"}};
        String [][]group2=new String[][]{{"Richard", "R"},{"Michael", "R"}};
        String [][][] Team1= new String[][][]{group1, group2};
        String [][]group3=new String[][]{{"Allen", "T"},{"Omer", "P"}};
        String [][]group4=new String[][]{{"David", "R"},{"Richard", "P"}};
        String [][][] Team2= new String[][][]{group3, group4};
        return new String[][][][]{Team1, Team2};
    }
    private String[][][][] get_tournament_empty() {
        String [][]group1=new String[][]{{"Armando", ""},{"Dave", ""}};
        String [][]group2=new String[][]{{"Richard", "R"},{"Michael", "R"}};
        String [][][] Team1= new String[][][]{group1, group2};
        String [][]group3=new String[][]{{"Allen", ""},{"Omer", "P"}};
        String [][]group4=new String[][]{{"David", "R"},{"Richard", ""}};
        String [][][] Team2= new String[][][]{group3, group4};
        return new String[][][][]{Team1, Team2};
    }
    private String[][][][] get_tournament_not_even() {
        String [][]group2=new String[][]{{"Richard", "R"},{"Michael", "R"}};
        String [][][] Team1= new String[][][]{group2};
        String [][]group3=new String[][]{{"Allen", ""},{"Omer", "P"}};
        String [][]group4=new String[][]{{"David", "R"},{"Richard", ""}};
        String [][][] Team2= new String[][][]{group3, group4};
        return new String[][][][]{Team1, Team2};
    }

    @Test
    public void verifyIfIsNotCorrectQuantity(){
        try{
            String result=rpt.tournament(get_tournament_not_even());
        }
        catch(WrongNumberPlayers e)
        {
            return;
        }
        catch (WrongPlayException e) {
            e.printStackTrace();
            return;
        }
        fail();
    }
    @Test
    public void verifyIfHasEmptyTeams(){
        try{
            String result=rpt.tournament(get_tournament_empty());
        }
        catch(WrongNumberPlayers e)
        {
            return;
        }
        catch (WrongPlayException e) {
            e.printStackTrace();
            return;
        }
        fail();
    }
    @Test
    public void isPotenceOfTwoShouldBeReturnTrue(){
        Assert.assertEquals(rpt.isPotenceOfTwo(2), true);
    }
    @Test
    public void isPotenceOfTwoShouldBeReturnFalse(){
        Assert.assertEquals(rpt.isPotenceOfTwo(3), false);
    }
}
